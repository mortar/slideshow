/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/slideshow
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-25
---------------------------------------------------------------------------- */
$('.m-slideshow').slideshow({
	debug: 		true
});

function load() {
	console.log('load');
}

function loaded() {
	console.log('loaded');
}

function changed(obj) {
	console.log('changed', obj);
}

function selected(obj) {
	console.log('selected', obj);
}