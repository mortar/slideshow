(function( $ ) {
 
    $.fn.slideshow = function(options) {

        var defaults = {   
            debug:      false,
            speed:      500,
            prev:       '.m-slideshow-prev',
            next:       '.m-slideshow-next',
            container:  '.m-slideshow-container',
            slide:      '.m-slideshow-slide'
        };
     
        var settings = $.extend( {}, defaults, options );

        var getSpeed = function(obj) {
            var speed = settings.speed;
            if(obj.data('speed')) {
                speed = obj.data('speed');
            }
            return speed;
        }

        return this.each(function() {
            var self = $(this);

            //determine transition speed
            var speed = getSpeed(self);

            var prevButton = settings.prev;
            if($(this).data('prev')) {
                prevButton = $(this).data('prev');
            }

            var nextButton = settings.next;
            if($(this).data('next')) {
                nextButton = $(this).data('next');
            }

            var slideIndex = 1;

            var showPrevious = function() {
                currentSlide = self.find(settings.slide).filter('[data-index="' + slideIndex + '"]');

                slideIndex--;

                if(slideIndex < 1) {
                    slideIndex = self.find(settings.slide).length;
                }

                nextSlide = self.find(settings.slide).filter('[data-index="' + slideIndex + '"]');

                renderSlide(currentSlide, nextSlide);
            }

            var showNext = function() {
                currentSlide = self.find(settings.slide).filter('[data-index="' + slideIndex + '"]');

                slideIndex++;

                if(slideIndex > self.find(settings.slide).length) {
                    slideIndex = 1;
                }

                nextSlide = self.find(settings.slide).filter('[data-index="' + slideIndex + '"]');

                renderSlide(currentSlide, nextSlide);
            }

            var renderSlide = function(currentSlide, nextSlide) {
                nextSlide.css({
                    'display': 'block',
                    'z-index': -1
                });

                if(currentSlide === null) {
                    nextSlide.addClass('active');

                    if(nextSlide.data('caption')) {
                        self.find('.m-slideshow-caption').html(nextSlide.data('caption')).fadeIn(speed);
                    }else{
                        self.find('.m-slideshow-caption').fadeOut(speed);
                    }

                    //run changed callback
                    if(self.data('changed')) {
                        window[self.data('changed')]();
                    }
                }else{
                    currentSlide.fadeOut(speed, function() {
                        nextSlide.addClass('active');
                        currentSlide.removeClass('active');

                        if(nextSlide.data('caption')) {
                            self.find('.m-slideshow-caption').html(nextSlide.data('caption')).fadeIn(speed);
                        }else{
                            self.find('.m-slideshow-caption').fadeOut(speed);
                        }

                        //run changed callback
                        if(self.data('changed')) {
                            window[self.data('changed')]();
                        }
                    });  
                }

            }


            /* INITIALISE -------------------------------- */

            //run load callback
            if(self.data('load')) {
                window[self.data('load')]();
            }

            // self.find(settings.slide).filter(':first').addClass('active');

            renderSlide(null, self.find(settings.slide).filter(':first'));

            //if scroll is set, automaticalling scroll through carousel
            if(self.data('scroll')) {
                setInterval(function(){
                    showNext();
                }, self.data('scroll'));
            }

            //on load, set images to remove img tags and set images as background of divs to be responsive
            self.find(settings.slide).each(function(i, val) {
                if($(this).data('src')) {
                    $(this).css({
                        'background-image': 'url("' + $(this).data('src')  + '")'
                    });
                }
                $(this).attr('data-index', i + 1);
            });

            //run loaded callback
            if(self.data('loaded')) {
                window[self.data('loaded')]();
            }


            /* EVENTS -------------------------------- */

            self.find(prevButton).off().on('click', function(e) {
                e.preventDefault();
                showPrevious();
            });

            self.find(nextButton).off().on('click', function(e) {
                e.preventDefault();
                showNext();
            });

        });
    };

}( jQuery ));